### Configuration

Commento is quite configurable, in both the frontend and the backend. You're free to inject custom styling CSS, JavaScript, toggle moderation settings, configure SMTP, enable OAuth, and so on. In this section, we document the exhaustive list of settings you can configure in Commento.

Configuring the backend and the frontend is done separately. See the corresponding page for more information on how to do so:

 - [Configuring the backend](configuration-backend.md): specify the server binding address, set up PostgreSQL, configure SMTP, enable OAuth, configure a CDN, enable compression of static assets, and more.
 - [Configuring the frontend](configuration-frontend.md): configure custom CSS styling, specify the `<div>` to fill, and more.
