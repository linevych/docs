### Getting Started

Commento is a Go-based application that uses PostgreSQL for the backend database. One of the benefits of Go is that it produces statically linked binaries that require no runtime environments (other than the operating system itself) or dependencies. This greatly simplifies distribution, deployment, and upgrades.

We strongly recommend you [review the system requirements](system-requirements.md) to run Commento. Following this, you can install Commento using any of the methods [listed here](installation.md).

Most instructions in this section are applicable to both CE and EE, unless otherwise specified.
