### Community Edition or Enterprise Edition

#### Introduction

Commento is software built on an open core model (inspired by [GitLab](https://gitlab.com)) that you can install and manage in your own premises. There are two editions of Commento: the Community Edition (CE) and the Enterprise Edition (EE).

CE is free software that is open source under the [MIT license](https://gitlab.com/commento/commento-ce/blob/master/LICENSE). It's built by collaborators from all around the world. EE is built on top of the CE, offering additional features and functionality geared towards organisations and enterprises, while using the same core as CE.

#### Reasons to choose the Enterprise Edition

The Enterprise Edition requires a subscription-based license to operate. Without such a license, the software will automatically fallback to CE's feature set, and you can continue using the same software package. For this reason, it's recommended you start with the Enterprise Edition as it makes upgrades easier, if you don't mind running proprietary code.

If you're an organisation or an enterprise requiring advanced tools (such as audit logs for moderation, for example), EE is the right choice for you. These additional features are mostly geared for larger audiences.

Another reason to choose EE is if you need official support and assistance integrating Commento into your product. All EE users automatically get priority support, with higher plans offering next business-day support.

#### Reasons to choose the Community Edition

The Community Edition is completely free software (free as in freedom *and* beer) that's licensed under the [MIT license](https://gitlab.com/commento/commento-ce/blob/master/LICENSE). If you prefer software to be free for ethical reasons, you will want to choose the Community Edition.

Note that CE does not come with official support or assistance intergating Commento into your product. You will, however, always be welcome to community support.

#### Alternative to both CE and EE

If you're not interested in hosting, managing, and updating servers, we also offer a hosted version of the product that you can simply embed. This is perfect for people hosting static websites, without access to a server to run the backend, or want access to pre-configured features such as HTTPS support and CDNs for speed.
