<p align="center">
<a href="https://commento.io"><img src="https://user-images.githubusercontent.com/7521600/33375172-14b21f68-d52f-11e7-9b30-477682bccf8f.png" width=300></a>
</p>

<p align="center"><b>An open source, privacy focused discussion platform.</b></p>

---

Welcome to the [**Commento**](https://commento.io) documentation! If you're here for the first time, read this page to get started. Otherwise, you can use the sidebar and the search on the left to find what you're looking for.

#### What is Commento?

Commento is the best way to foster discussion on your website without compromising your readers' privacy. It's fast, modern, featureful, secure, and easy to embed. You can install Commento on your blog, news articles, and any place where you want your readers to add comments. Unlike most alternatives, Commento is lightweight and privacy focused; we'll never sell your data, show ads, embed third-party tracking scripts, or inject affiliate links.

#### Editions

There are three editions of Commento. If you need help choosing between CE and EE, [read this](ce-or-ee.md).

 - **Commento Community Edition** (CE) is open source software that's freely available under the MIT Expat license.
 - **Commento Enterprise Edition** (EE) is built on top of CE and includes extra features geared towards organizations.
 - [**Commento Hosted**](https://commento.io) is a service version of Commento if you want to embed Commento without running servers.  This is currently in private beta and you can [add yourself to the waiting list here](https://commento.io).
