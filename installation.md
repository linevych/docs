### Installation

There are numerous ways to get Commento installed and running on your website. We strongly recommend installing Commento with [Docker](https://docker.com) and [Docker Compose](https://docs.docker.com/compose/) since it is painless to install, upgrade, and is generally more reliable. Here is the list of installation methods, in the order of recommendation:

 - **[Install with Docker](installation-docker.md) (recommended)**
 - **[Install using releases](installation-releases.md)**
 - **[Install from source](installation-source.md)**
